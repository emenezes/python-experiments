

import time
#import matplotlib.pyplot as plt
from simple_pid import PID


class Uav:
    def __init__(self):
        self.pos = 0
        self.vel = 0

    def update(self, dt):
        self.pos += self.vel*dt
        return self.pos


if __name__ == '__main__':
    uav1 = Uav()
    uav1.pos = 20
    uav1.vel = 10
    uav2 = Uav()
    dist = uav1.pos - uav2.pos

    pid = PID(5, 0.01, 0.1, setpoint=0)
    #pid.output_limits = (1, None)

    start_time = time.time()
    last_time = start_time

    # keep track of values for plotting
    setpoint, y1, y2, v_y2, d, x = [], [], [], [], [], []

    while time.time() - start_time < 20:
        current_time = time.time()
        dt = current_time - last_time
        
        dist = uav1.pos - uav2.pos
        uav2.vel = -1*pid(dist)
        uav1.update(dt)
        uav2.update(dt)
        dist = uav1.pos - uav2.pos

        x += [current_time-start_time]
        y1 += [uav1.pos]
        y2 += [uav2.pos]
        v_y2 += [uav2.vel]
        d += [dist]
        setpoint += [pid.setpoint]

        last_time = current_time
        time.sleep(0.1)
    
    #print(x, y, setpoint)
    import csv
    with open('mycsv.csv', 'w', newline='') as f:
        thewriter = csv.writer(f)
        print("Printing X:", len(x), "elements")
        thewriter.writerow(x)
        print("Printing Y:", len(d), "elements")
        thewriter.writerow(y1)
        thewriter.writerow(y2)
        thewriter.writerow(d)
        thewriter.writerow(v_y2)
        print("Printing setpoint:", len(setpoint), "elements")
        thewriter.writerow(setpoint)

    #plt.plot(x, y, label='measured')
    #plt.plot(x, setpoint, label='target')
    #plt.xlabel('time')
    #plt.ylabel('temperature')
    #plt.legend()
    #plt.show()
