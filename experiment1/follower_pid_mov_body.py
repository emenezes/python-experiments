
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
© Copyright 2015-2016, 3D Robotics.
guided_set_speed_yaw.py: (Copter Only)

This example shows how to move/direct Copter and send commands in GUIDED mode using DroneKit Python.

Example documentation: http://python.dronekit.io/examples/guided-set-speed-yaw-demo.html
"""
from __future__ import print_function

from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil # Needed for command message definitions
import time
import math, numpy as np
import threading
from dronekit_support import *


#Set up option parsing to get connection string
import argparse
parser = argparse.ArgumentParser(description='Control Copter and send commands in GUIDED mode ')
parser.add_argument('--connect',
                   help="Vehicle connection target string. If not specified, SITL automatically started and used.")
parser.add_argument('--n', help="Nr of Vehicles")
args = parser.parse_args()

connection_string = args.connect
nr_vehicles = int(args.n)
nr_particles = nr_vehicles-1
sitl = None

MAX_VEL = 20
homeLocation = LocationGlobal(-22.904997,-43.16309,5)

#Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

# Connect to the Vehicle
basePort = int( connection_string.split(":")[2] )
vehicles = [connect('tcp:127.0.0.1:' + str(basePort + 10 * (i+1))) for i in range(nr_vehicles)]
vehicle = vehicles[0]
vehicle2 = vehicles[1]
for v in vehicles:
    v.home_location = homeLocation

#Arm and take of to altitude of 5 meters
desiredAltitude = 5
for aVehicle in vehicles:
    arm_and_takeoff_no_wait(aVehicle, desiredAltitude)

while True:
    allVehiclesReachedGoal = True
    for aVehicle in vehicles:
        if aVehicle.location.global_relative_frame.alt < desiredAltitude*0.9:
            allVehiclesReachedGoal = False
    if allVehiclesReachedGoal:
        print("All vehicles tookoff")
        break

#for aVehicle in vehicles:
#    aVehicle.groundspeed = 10

leaderThread = LeaderThread(1, vehicle)
leaderThread.start()


#import pso
sample_time = 0.1
from simple_pid import PID
for aVehicle in vehicles:
    aVehicle.pid = PID(0.01, 0.0001, 0.001, setpoint=5)
    aVehicle.pid.sample_time = sample_time
    aVehicle.pid.output_limits = (-1, 1)

time.sleep(7)

def moveInBodyNED(aVehicle, north, east, down, aGroundSpeed=5):
    aVehicle.groundspeed = aGroundSpeed
    msg = aVehicle.message_factory.set_position_target_local_ned_encode(
        0, # time_boot_ms (not used)
        0, 0, # target system, target component
        mavutil.mavlink.MAV_FRAME_BODY_NED, # frame
        0b0000111111111000, # type_mask (only positions enabled)
        north, east, down, # x, y, z positions (or North, East, Down in the MAV_FRAME_BODY_NED frame
        0, 0, 0, # x, y, z velocity in m/s  (not used)
        0, 0, 0, # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0) # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    # send command to vehicle
    aVehicle.send_mavlink(msg)

while leaderThread.isAlive and leaderThread.isRunning:
    currentLocations = [aVehicle.location.global_relative_frame for aVehicle in vehicles]
    for i in range(1, nr_vehicles):
        direction = get_bearing(currentLocations[i], currentLocations[0])
        condition_yaw(vehicles[i], direction)
        distance = get_distance_metres(currentLocations[i], currentLocations[0])
        power = aVehicle.pid(distance)
        moveInBodyNED(vehicles[i], distance, 0, 0, MAX_VEL)
        print("Vehicle[", i+1, "].direction: ", direction, ".velocity: ", MAX_VEL * -1 * power, "distance: ", distance)

    time.sleep(sample_time)


leaderThread.join()


"""
The example is completing. LAND at current location.
"""

for i in range(nr_vehicles):
    print("Setting LAND mode for vehicle ", i)
    vehicles[i].mode = VehicleMode("RTL")
    vehicles[i].close()


# Shut down simulator if it was started.
if sitl is not None:
    sitl.stop()

print("Completed")
