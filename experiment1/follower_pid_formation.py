
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
© Copyright 2015-2016, 3D Robotics.
guided_set_speed_yaw.py: (Copter Only)

This example shows how to move/direct Copter and send commands in GUIDED mode using DroneKit Python.

Example documentation: http://python.dronekit.io/examples/guided-set-speed-yaw-demo.html
"""
from __future__ import print_function

from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil # Needed for command message definitions
import time
import math, numpy as np
import threading
from dronekit_support import *


#Set up option parsing to get connection string
import argparse
parser = argparse.ArgumentParser(description='Control Copter and send commands in GUIDED mode ')
parser.add_argument('--connect',
                   help="Vehicle connection target string. If not specified, SITL automatically started and used.")
parser.add_argument('--n', help="Nr of Vehicles")
args = parser.parse_args()

connection_string = args.connect
nr_vehicles = int(args.n)
nr_particles = nr_vehicles-1
sitl = None

MAX_VEL = 20
SAFE_DISTANCE = 5
homeLocation = LocationGlobal(-22.904997,-43.16309,5)

#Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

# Connect to the Vehicle
basePort = int( connection_string.split(":")[2] )
vehicles = [connect('tcp:127.0.0.1:' + str(basePort + 10 * (i+1))) for i in range(nr_vehicles)]
vehicle = vehicles[0]
vehicle2 = vehicles[1]
for v in vehicles:
    v.home_location = homeLocation

#Arm and take of to altitude of 5 meters
desiredAltitude = 5
for aVehicle in vehicles:
    arm_and_takeoff_no_wait(aVehicle, desiredAltitude)

while True:
    allVehiclesReachedGoal = True
    for aVehicle in vehicles:
        if aVehicle.location.global_relative_frame.alt < desiredAltitude*0.9:
            allVehiclesReachedGoal = False
    if allVehiclesReachedGoal:
        print("All vehicles tookoff")
        break

#for aVehicle in vehicles:
#    aVehicle.groundspeed = 10

leaderThread = LeaderThread(1, vehicle)
leaderThread.start()



SAMPLE_TIME = 0.1
from simple_pid import PID
for aVehicle in vehicles:
    #aVehicle.pid = PID(0.01, 0.0001, 0.001, setpoint=0)
    aVehicle.pid = PID(0.0061, 0.0001, 0.001, setpoint=0)
    aVehicle.pid.sample_time = SAMPLE_TIME
    aVehicle.pid.output_limits = (-1, 1)

time.sleep(7)

SAFE_DISTANCE = 10
def getIdealPosition(leaderGobalPosition, vehicleNr):
    switcher = {
        1: leaderGobalPosition,
        2: get_location_metres(leaderGobalPosition, -SAFE_DISTANCE, 0),
        3: get_location_metres(leaderGobalPosition, -SAFE_DISTANCE, -SAFE_DISTANCE),
        4: get_location_metres(leaderGobalPosition, -SAFE_DISTANCE, SAFE_DISTANCE)
    }
    return switcher.get(vehicleNr, leaderGobalPosition)

while leaderThread.isAlive and leaderThread.isRunning:
    currentLocations = [aVehicle.location.global_relative_frame for aVehicle in vehicles]
    idealLocations = [getIdealPosition(currentLocations[0], i+1) for i in range(0, nr_vehicles)]
    for i in range(1, nr_vehicles):
        direction = get_bearing(currentLocations[i], idealLocations[i-1])
        condition_yaw(vehicles[i], direction)
        distance = get_distance_metres(currentLocations[i], idealLocations[i-1])
        power = aVehicle.pid(distance)
        send_ned_velocity_forward(vehicles[i], MAX_VEL * -1 * power)
        print("Vehicle[", i+1, "].direction: ", direction, ".velocity: ", MAX_VEL * -1 * power, "distance: ", distance)

    time.sleep(SAMPLE_TIME)


leaderThread.join()


"""
The example is completing. LAND at current location.
"""

for i in range(nr_vehicles):
    print("Setting LAND mode for vehicle ", i)
    vehicles[i].mode = VehicleMode("RTL")
    vehicles[i].close()


# Shut down simulator if it was started.
if sitl is not None:
    sitl.stop()

print("Completed")
