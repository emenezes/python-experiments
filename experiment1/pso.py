#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
© Copyright 2015-2016, 3D Robotics.
guided_set_speed_yaw.py: (Copter Only)

This example shows how to move/direct Copter and send commands in GUIDED mode using DroneKit Python.

Example documentation: http://python.dronekit.io/examples/guided-set-speed-yaw-demo.html
"""
from __future__ import print_function

from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil # Needed for command message definitions
import time
import math, numpy as np
import threading
from dronekit_support import *


#Set up option parsing to get connection string
import argparse
parser = argparse.ArgumentParser(description='Control Copter and send commands in GUIDED mode ')
parser.add_argument('--connect',
                   help="Vehicle connection target string. If not specified, SITL automatically started and used.")
parser.add_argument('--n', help="Nr of Vehicles")
args = parser.parse_args()

connection_string = args.connect
nr_vehicles = int(args.n)
nr_particles = nr_vehicles-1
sitl = None

homeLocation = LocationGlobal(-22.904997,-43.16309,5)

#Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

# Connect to the Vehicle
basePort = int( connection_string.split(":")[2] )
vehicles = [connect('tcp:127.0.0.1:' + str(basePort + 10 * (i+1))) for i in range(nr_vehicles)]
vehicle = vehicles[0]
vehicle2 = vehicles[1]
for v in vehicles:
    v.home_location = homeLocation

#Arm and take of to altitude of 5 meters
desiredAltitude = 5
for aVehicle in vehicles:
    arm_and_takeoff_no_wait(aVehicle, desiredAltitude)

while True:
    allVehiclesReachedGoal = True
    for aVehicle in vehicles:
        if aVehicle.location.global_relative_frame.alt < desiredAltitude*0.9:
            allVehiclesReachedGoal = False
    if allVehiclesReachedGoal:
        print("All vehicles tookoff")
        break

#for v in vehicles:
#    goto(v, 0, -50, v.simple_goto)

leaderThread = LeaderThread(1, vehicle)
leaderThread.start()


for aVehicle in vehicles:
    aVehicle.groundspeed = 1



import pso
pso.test()
#x=1
#pso.test_with_obj_1(x)
#print(x)
#fList = [0.1,0.2,0.3]
#pso.test_with_obj_2(fList)
#print(fList)
#pso.test_with_obj_3(fList)
#print(fList)
#pso.test_with_obj_4(fList)
#print(fList)
#arr = np.zeros(nr_vehicles)
#pso.test_with_obj_4(arr)
#print(arr)
#arr = np.zeros(nr_vehicles)
#pso.testMutatingObj(arr, nr_particles)
#print(arr)

positions = np.zeros(nr_vehicles * 2)
velocities = np.zeros(nr_particles * 2)


while leaderThread.isAlive and leaderThread.isRunning:
    currentLocations = [aVehicle.location.local_frame for aVehicle in vehicles]

    for i in range(0, nr_vehicles):
        positions[2*i] = currentLocations[i].north
        positions[2*i+1] = currentLocations[i].east

    ## void ParticleSwarmOptimization(float dest_x, float dest_y, float target, int n_particles, float c1, float c2, float att, float rep, float W, double positions[24], double last_pbest[36], double vel[24], double debug[20]) {
    #psoLib.ParticleSwarmOptimization(ctypes.c_float(currentLocations[0].north),ctypes.c_float(currentLocations[0].east),target,ctypes.c_int(nr_particles),c1,c2,att,rep,W,positions,lastBestPositions,velocities,debugData)
    ## psoLib.ParticleSwarmOptimization1(positions, velocities, nr_particles)
    pso.pso_with_defaults(positions, velocities, nr_particles)

    for i in range(1, nr_vehicles):
        j=i-1
        north = positions[2*i]
        east = positions[2*i+1]
        down = currentLocations[i].down
        #print("Vehicle[", i+1, "].position: ", north, east, down)
        #send_ned_velocity(vehicles[i], velocities[2*j], velocities[2*j+1], 0)
        goto_position_target_local_ned(vehicles[i], north, east, down)
        speed = math.sqrt(velocities[2*j]**2 + velocities[2*j+1]**2)
        #print("Vehicle[", i+1, "].groundspeed = ", speed, " m/s")
        #vehicles[i].groundspeed = speed

    time.sleep(1)


leaderThread.join()


"""
The example is completing. LAND at current location.
"""

for i in range(nr_vehicles):
    print("Setting LAND mode for vehicle ", i)
    vehicles[i].mode = VehicleMode("RTL")
    vehicles[i].close()


# Shut down simulator if it was started.
if sitl is not None:
    sitl.stop()

print("Completed")


#import os, ctypes
#psoLibPath = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "PSO_dll.dll"
#print(psoLibPath)
## psoLibPath = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "libpso.so"
#psoLib = ctypes.cdll.LoadLibrary(psoLibPath)

## test1 = ctypes.WINFUNCTYPE(None)
## test2 = test1(("ParticleSwarmOptimization",psoLib))
## print(test2)
## test2.PsoTest()
#psoLib.PsoTest()
## testParams = (ctypes.c_double * 30)()
## for i in range(30):
##     testParams[i] = i + i/100
## psoLib.PsoTest2(testParams, ctypes.c_int(30))
## for i in range(30):
##     print(testParams[i], end=" ")

#positions = (ctypes.c_double * 24)()
#velocities = (ctypes.c_double * 24)()
## positions = (ctypes.c_double * (nr_vehicles * 2))()
## velocities = (ctypes.c_double * (nr_particles * 2))()

#for i in range(nr_particles * 2):
#    velocities[i] = 0
#    print(velocities[i], end=" ")
## psoLib.PsoTest2(velocities, ctypes.c_int(nr_particles * 2))
## for i in range(nr_particles * 2):
##     print(velocities[i], end=" ")

#time.sleep(5)

#target = ctypes.c_float(10)
#c1 = ctypes.c_float(0.1)
#c2 = ctypes.c_float(4)
#att = ctypes.c_float(5)
#rep = ctypes.c_float(1)
#W = ctypes.c_float(1)
#lastBestPositions = (ctypes.c_double * 36)()
## lastBestPositions = (ctypes.c_double * (nr_particles * 3))()
#for i in range(nr_particles * 3):
#    lastBestPositions[i] = math.inf
#debugData = (ctypes.c_double * 20)()