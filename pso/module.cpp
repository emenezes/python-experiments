#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include "Pso.h"

namespace py = pybind11;

struct DynamicData {
	double* position;
	double* velocity;
};

void test_with_obj_1(py::int_ x) {
	x = 5;
}

void test_with_obj_2(double a[], size_t s) { // recebe c�pia do valor
	std::cout << "C++ test_with_obj_2 " << a << std::endl;
	a[0] = 2;
	std::cout << "C++ test_with_obj_2 " << a << std::endl;
}

void test_with_obj_3(py::list a) { // recebe o pr�prio objeto python
	std::cout << "C++ test_with_obj_3 " << a << std::endl;
	a[0] = 3; // Tb altera no python
	std::cout << "C++ test_with_obj_3 " << a << std::endl;
}

void test_with_obj_4(py::array_t<double> a) { // recebe c�pia da lista python e referencia do numpy array
	std::cout << "C++ test_with_obj_4 " << a << std::endl;
	double * d = a.mutable_data();
	d[0] = 4.1; // Tb altera no python, se for numpy array
	std::cout << "C++ test_with_obj_4 " << a << std::endl;
}

PyObject* pso_with_defaults(PyObject* self, PyObject* args) {
	PyObject *positionsListObj;
	PyObject *velocityListObj;
	int nrParticles;
	if (!PyArg_ParseTuple(args, "OOi", &positionsListObj, &velocityListObj, &nrParticles)) {
		return NULL;
	}
	return Py_BuildValue("i", 0);
}


PYBIND11_MODULE(pso, m) {
	py::class_<DynamicData>(m, "DynamicData")
		.def_readwrite("position", &DynamicData::position)
		.def_readwrite("velocity", &DynamicData::velocity)
		.def("__repr__", [](const DynamicData &a) {
				return "<DynamicData ' ... '>";
		});
	m.def("test", []() { 
		PsoTest();
	});
	m.def("testMutatingObj", [](py::array_t<double> positions, py::int_ nrParticles) {
		double *pos = positions.mutable_data();
		PsoTest2(pos, nrParticles);
	});
	m.def("test_with_obj_1", &test_with_obj_1);
	m.def("test_with_obj_2", [](std::vector<double> array) {
		return test_with_obj_2(array.data(), array.size());
	});
	m.def("test_with_obj_3", &test_with_obj_3);
	m.def("test_with_obj_4", &test_with_obj_4);
	m.def("pso_with_defaults", [](py::array_t<double> positions, py::array_t<double>velocities, py::int_ nrParticles) {
		double *pos = positions.mutable_data();
		double *vel = velocities.mutable_data();
		//std::cout << "Pos- " << positions << std::endl;
		std::cout << positions << std::endl;
		ParticleSwarmOptimization1(pos, vel, nrParticles);
		//std::cout << "Pos+ " << positions << std::endl;
	});


	m.attr("__version__") = "dev";
}


//static PyMethodDef pso_methods[] = {
//	// The first property is the name exposed to Python, fast_tanh, the second is the C++
//	// function name that contains the implementation.
//	{ "tanh", (PyCFunction)tanh_impl, METH_O, nullptr },
//	{ "test", (PyCFunction)test, METH_NOARGS, nullptr },
//	{ "test_with_obj_1", (PyCFunction)test_with_obj_1, METH_VARARGS, nullptr },
//	{ "pso_with_defaults", (PyCFunction)pso_with_defaults, METH_VARARGS, nullptr },
//
//	// Terminate the array with an object containing nulls.
//	{ nullptr, nullptr, 0, nullptr }
//};
//
//static PyModuleDef pso_module = {
//	PyModuleDef_HEAD_INIT,
//	"pso",                        // Module name to use with Python import statements
//	"Particle Swarm Optimization",  // Module description
//	0,
//	pso_methods                   // Structure that defines the methods of the module
//};
//
//PyMODINIT_FUNC PyInit_pso() {
//	return PyModule_Create(&pso_module);
//}
