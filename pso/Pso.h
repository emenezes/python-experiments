#pragma once

#ifndef __PSO_H
#define __PSO_H

#if !defined(unix) && !defined(__unix__) && !defined(__unix)
#define DLLEXPORT
#else
#define DLLEXPORT extern "C"
#endif

DLLEXPORT void PsoTest();
DLLEXPORT void PsoTest2(double positions[], int nrParticles);
DLLEXPORT void ParticleSwarmOptimization1(double positions[], double vel[], int n_particles);
DLLEXPORT void ParticleSwarmOptimization(float dest_x, float dest_y, float target, int n_particles, float c1, float c2, float att, float rep, float W, double positions[], double last_pbest[], double vel[], double debug[]);

#endif
